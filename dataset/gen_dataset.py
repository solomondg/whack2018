from tqdm import tqdm
import pandas as pd
import numpy as np
from collections import namedtuple
from math import floor
from scipy.ndimage.filters import gaussian_filter
import pickle
from keras.optimizers import SGD


after2015Path = 'after2015/crime.csv'
with open(after2015Path, 'r', encoding="ISO-8859-1") as f:
    after2015csv = f.read()
before2015Path = 'before2015/crime-incident-reports-july-2012-august-2015-source-legacy-system.csv'
with open(before2015Path, 'r', encoding="ISO-8859-1") as f:
    before2015csv = f.read()

CrimeAfter2015Point = namedtuple('Crime2015Point', ['INCIDENT_NUMBER',
                                                    'OFFENSE_CODE',
                                                    'OFFENSE_CODE_GROUP',
                                                    'OFFENSE_DESCRIPTION',
                                                    'DISTRICT',
                                                    'REPORTING_AREA',
                                                    'SHOOTING',
                                                    'OCCURED_ON_DATE',
                                                    'YEAR',
                                                    'MONTH',
                                                    'DAY_OF_WEEK',
                                                    'HOUR',
                                                    'UCR_PART',
                                                    'STREET',
                                                    'LAT',
                                                    'LONG',
                                                    'LOCATION'
                                                    ]
                                 )


CrimeBefore2015Point = namedtuple('Crime2012Point', ['COMPNOS',
                                                     'NATURECODE',
                                                     'INCIDENT_TYPE_DESCRIPTION',
                                                     'MAIN_CRIMECODE',
                                                     'REPTDISTRICT',
                                                     'REPORTINGAREA',
                                                     'FROMDATE',
                                                     'WEAPONTYPE',
                                                     'SHOOTING',
                                                     'DOMESTIC',
                                                     'SHIFT',
                                                     'YEAR',
                                                     'MONTH',
                                                     'DAY_WEEK',
                                                     'UCRPART',
                                                     'X',
                                                     'Y',
                                                     'STREETNAME',
                                                     'XSTREETNAME',
                                                     'LOCATION'
                                                     ]
                                  )

before2015csv = before2015csv.strip('\"')
after2015csv = after2015csv.strip('\"')
before2015csv = before2015csv.split('\n')[1:-1]
after2015csv = after2015csv.split('\n')[1:-1]
before2015list = [line.split(',') for line in before2015csv]
after2015list = [line.split(',') for line in after2015csv]

before2015points = []

for n in tqdm(range(len(before2015list))):
    i = before2015list[n]
    before2015points.append(
        CrimeBefore2015Point(
            COMPNOS=i[0],
            NATURECODE=i[1],
            INCIDENT_TYPE_DESCRIPTION=i[2],
            MAIN_CRIMECODE=i[3],
            REPTDISTRICT=i[4],
            REPORTINGAREA=i[5],
            FROMDATE=i[6],
            WEAPONTYPE=i[7],
            SHOOTING=i[8],
            DOMESTIC=i[9],
            SHIFT=i[10],
            YEAR=i[11],
            MONTH=i[12],
            DAY_WEEK=i[13],
            UCRPART=i[14],
            X=i[15],
            Y=i[16],
            STREETNAME=i[17],
            XSTREETNAME=i[18],
            LOCATION=i[19] + "," + i[20]
        )
    )

after2015points = []

for n in tqdm(range(len(after2015list))):
    i = after2015list[n]
    after2015points.append(
        CrimeAfter2015Point(
            INCIDENT_NUMBER=i[0],
            OFFENSE_CODE=i[1],
            OFFENSE_CODE_GROUP=i[2],
            OFFENSE_DESCRIPTION=i[3],
            DISTRICT=i[4],
            REPORTING_AREA=i[5],
            SHOOTING=i[6],
            OCCURED_ON_DATE=i[7],
            YEAR=i[8],
            MONTH=i[9],
            DAY_OF_WEEK=i[10],
            HOUR=i[11],
            UCR_PART=i[12],
            STREET=i[13],
            LAT=i[14],
            LONG=i[15],
            LOCATION=i[16] + "," + i[17]
        )
    )


CrimePoint = namedtuple("CrimePoint", ['CRIME', 'CODE', 'LAT', 'LONG'])

crimes = []

beforeRejects = []
afterRejects = []

for i in before2015points:
    lat, lon = [float(n) for n in i.LOCATION.strip('(').strip(')').split(',')]

    if lat == 0.0 or lon == 0.0:
        beforeRejects.append(i)
        continue

    crimes.append(
        CrimePoint(
            CRIME=i.INCIDENT_TYPE_DESCRIPTION,
            CODE=i.MAIN_CRIMECODE,
            LAT=lat,
            LONG=lon,
        )
    )

for i in after2015points:
    try:
        lat, lon = \
            [float(n) for n in i.LOCATION.strip('(').strip(')').split(',')]
    except:
        afterRejects.append(i)
        continue

    if lat == 0.0 or lon == 0.0 or lat == -1 or lon == -1:
        afterRejects.append(i)
        continue

    crimes.append(
        CrimePoint(
            CRIME=i.OFFENSE_DESCRIPTION,
            CODE=i.OFFENSE_CODE,
            LAT=lat,
            LONG=lon,
        )
    )

crimeSet = set()
for i in crimes:
    crimeSet.add(i.CRIME)


with open('crime_classification.txt', 'r', encoding="ISO-8859-1") as f:
    crimeClassificationTxt = f.read()

crimeClassification = {}

for i in crimeClassificationTxt.split('\n')[:-1]:
    i = i.encode('ascii', 'ignore').decode('utf-8')
    crimeClassification[i[2:]] = i[0]


violentCrimes = []
drugCrimes = []
otherCrimes = []

#print(crimeClassification.keys())

for i in crimes:
    if 'ACCIDENT - PROPERTY' in i.CRIME or \
            'ACCIDENT - INVOLVING' in i.CRIME or \
            'VAL - OPERATING UNREG' in i.CRIME:
        continue
    cr = crimeClassification[i.CRIME]
    if cr == 'O':
        otherCrimes.append(i)
    elif cr == 'D':
        drugCrimes.append(i)
    elif cr == 'V':
        violentCrimes.append(i)

lats = np.asarray([i.LAT for i in crimes])
longs = np.array([i.LONG for i in crimes])


lat_min = np.min(lats)
lat_max = np.max(lats)
long_min = np.min(longs)
long_max = np.max(longs)

long_diff = long_max - long_min
lat_diff = lat_max - lat_min

#print(lat_min, lat_max)
#print(long_min, long_max)
#print(lat_max-lat_min, long_max-long_min)

div = 1000

arr = np.zeros((div, div))


def latNorm(lat): return (lat-lat_min)/lat_diff


def longNorm(lon): return (lon-long_min)/long_diff


def latDeNorm(lat): return lat*lat_diff + lat_min


def longDeNorm(lon): return lon*long_diff + long_min

for i in crimes:
    lat, lon = i.LAT, i.LONG
    latProc = latNorm(lat)
    longProc = longNorm(lon)
    latmul = latProc*div
    lonmul = longProc*div
    latIdx = int(floor(latmul)) - 1
    longIdx = int(floor(lonmul)) - 1
    arr[latIdx][longIdx] += 1

arr_smooth = gaussian_filter(arr, sigma=3)

#with open('all_crimes_smooth.pkl', 'wb') as f:
#    pickle.dump(arr_smooth, f)

def idxToLat(idx):
    return latDeNorm(idx/div)

def idxToLon(idx):
    return longDeNorm(idx/div)

def idxToLatLon(idxList):
    return idxToLat(idxList[0]), idxToLon(idxList[1])

import pickle
import numpy as np
import scipy as sp

import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, Flatten, Reshape

# Model input: Lat + Long
# Model output: Prediction of (gauss smoothed) crime at point

#with open('./all_crimes_smooth.pkl', 'rb') as f:
#    dset = pickle.load(f)

dset = arr_smooth
#dset = arr


def gen_model() -> Sequential:
    model = Sequential(
        [
            Dense(128, input_shape=(2,)),
            Activation('relu'),
            Dense(512),
            Activation('relu'),
            Dense(256),
            Activation('relu'),
            Dense(128),
            Activation('relu'),
            Dense(16),
            Activation('relu'),
            Dense(1),
            Activation('relu'),
        ]
    )
    return model

model = gen_model()

sgd = SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=False)

model.compile(
    optimizer=sgd,
    #loss='mean_squared_logarithmic_error',
    loss='mse'
)

X = []
Y = []

for i in range(dset.shape[0]):
    for j in range(dset.shape[1]):
        X.append([i, j])
        Y.append(dset[j][i])

X = np.asarray(X)
Y = np.asarray(Y)
X = X / div
Y = Y / np.max(Y)

#print(idxToLatLon(X[0]), Y[0])
#j = np.argmax(Y)
#print(idxToLatLon(X[j]), Y[j])

print(X.shape)
print(Y.shape)

model.fit(X, Y, epochs=2, batch_size=500)

toRegress = []

for i in range(div):
    for j in range(div):
        toRegress.append([i, j])

toRegress = np.asarray(toRegress)

predictions = model.predict(toRegress)

print(toRegress.shape, predictions.shape)

coupled = []
for i in range(toRegress.shape[0]):
    coupled.append([idxToLat(toRegress[i][0]), idxToLon(toRegress[i][1]), predictions[i][0]])

csv = ""
for i in coupled:
    csv += str(i[0]) + "," + str(i[1]) + "," + str(i[2]) + "\n"

for i in coupled[::100]:
    print(i)

with open('regress.pkl', 'wb') as f:
    pickle.dump(coupled, f)
with open('regress.csv', 'w') as f:
    f.write(csv)

print("Max: ", Y.argmax(), Y[Y.argmax()])
print("NN Max: ", Y.argmax(), coupled[Y.argmax()])

coupled_HM = []
for i in range(X.shape[0]):
    coupled_HM.append([idxToLat(X[i][0]*div), idxToLon(X[i][1]*div), Y[i]])

csv = ""
for i in coupled_HM:
    csv += str(i[0]) + "," + str(i[1]) + "," + str(i[2]) + "\n"
with open('heat.csv', 'w') as f:
    f.write(csv)


#with open("regress.pkl", 'wb') as f:
#    pickle.dump(predictions, f)

image = np.zeros((div, div, 4))

import cv2

m = 0
for i in range(arr_smooth.shape[0]):
    for j in range(arr_smooth.shape[1]):
        if arr_smooth[i][j] > m:
            m = arr_smooth[i][j]

arr_smooth /= m
for i in range(arr_smooth.shape[0]):
    for j in range(arr_smooth.shape[1]):
        image[j][i][0] = 25
        image[j][i][1] = 0
        image[j][i][2] = 170
        image[j][i][3] = arr_smooth[i][j]*255 if arr_smooth[i][j]>0.2 else 0

cv2.imwrite('image.png', arr_smooth)
cv2.imwrite('image_ri.png', arr)
cv2.imwrite('color.png', image)
