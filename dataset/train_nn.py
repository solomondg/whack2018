import pickle
import numpy as np
import scipy as sp

import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, Flatten, Reshape

# Model input: Lat + Long
# Model output: Prediction of (gauss smoothed) crime at point

with open('./all_crimes_smooth.pkl', 'rb') as f:
    dset = pickle.load(f)


def gen_model() -> Sequential:
    model = Sequential(
        [
            Dense(32, input_shape=(2,)),
            Activation('relu'),
            Dense(128),
            Activation('relu'),
            Dense(128),
            Activation('relu'),
            Dense(8),
            Activation('relu'),
            Dense(1),
            Activation('sigmoid'),
        ]
    )
    return model

model = gen_model()

model.compile(
    optimizer='rmsprop',
    loss='mse',
    metrics=['accuracy']
)

X = []
Y = []

for i in range(dset.shape[0]):
    for j in range(dset.shape[1]):
        X.append([i, j])
        Y.append(dset[j][i])

print(X[0], Y[0])
